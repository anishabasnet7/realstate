from django.db import models
from realtors.models import Realtor

# Create your models here.
class Listing(models.Model):
	merchant=models.ForeignKey(Realtor, on_delete=models.DO_NOTHING)
	title=models.CharField(max_length=255) 
	address=models.CharField(max_length=255)
	description=models.TextField(blank=True)
	hall=models.CharField(max_length=255)
	sqft=models.DecimalField(max_digits=2,decimal_places=2)
	compound=models.BooleanField(default=True)
	rooms=models.IntegerField()
	road=models.DecimalField(max_digits=2,decimal_places=2)
	floors=models.IntegerField()
	bathroom=models.IntegerField()
	hall=models.BooleanField(default=False)
	kitchen=models.IntegerField()
	garage=models.BooleanField(default=False)
	photo=models.ImageField(upload_to="photo/%Y/%m/%d")
	photo_1=models.ImageField(upload_to="photo/%Y/%m/%d",blank=True)
	photo_2=models.ImageField(upload_to="photo/%Y/%m/%d",blank=True)
	photo_3=models.ImageField(upload_to="photo/%Y/%m/%d",blank=True)
	photo_4=models.ImageField(upload_to="photo/%Y/%m/%d",blank=True)
	photo_5=models.ImageField(upload_to="photo/%Y/%m/%d",blank=True)
	def __str__(self):
			return self.title

