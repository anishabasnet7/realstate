from django.shortcuts import render
from listings.models import Listing
# Create your views here.
def index(request):
	popularListings=Listing.objects.all()[:8]
	latestListings=Listing.objects.all().order_by('-id')[:8]
	context={
			"popularListings": popularListings,
			"latestListings":latestListings,
			"title":"Ghar Aagan"
					
	}
	return render (request,"pages/index.html", context)


	# realtors= Realtor.objects.all()
	# context={
	# 	"title":"Ghar Aagan",
		
	# }
	
def search(request):
	Keyword=request.POST['Keyword']
	realtors=Realtor.objects.filter(name__contains=Keyword)
	context={
		"Keyword":Keyword,
		"realtors": realtors
		
	}
	return render (request,"pages/search.html", context)
