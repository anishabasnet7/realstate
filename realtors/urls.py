from django.urls import path
from .import views
urlpatterns=[
	path("", views.listRealtors,name="listRealtors"),
	path("<int:realtors>", views.viewRealtor,name="viewRealtor"),
]